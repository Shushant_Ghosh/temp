import boto3


def insert_dynamodb_data(event, context):
    dynamodb = boto3.resource('dynamodb')
    try:
        dynamoTable = dynamodb.Table('Test_Table')
        dynamoTable.put_item(
            Item={
                'Name': event["Name"],
                'Age': event["Age"],
                'Location': event["Location"]

            })
        message = "Record created Successfully"
        return message
    except Exception :
        message = "Record Creation Failed"
        return message

